# mcf-constants

Goal: auto deployment on merge to master

## Postgres
- [ ] ops-base
- [ ] qa/uat
- [ ] prod

## Deployment
- [ ] setup ci
- [ ] build docker image with application
- [ ] kubernetes description using k8stype
- [ ] database migration (with knex and see https://github.com/GovTechSG/mcf-boilerplate-js/tree/master/packages/constants to know what to integrate)
- [ ] jwt integration